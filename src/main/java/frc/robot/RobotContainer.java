// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.ArrayList;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.POVButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Constants.kIO;
import frc.robot.commands.DeployIntakeCommand;
import frc.robot.commands.PlayMusicCommand;
import frc.robot.commands.RetractIntakeCommand;
import frc.robot.commands.RunIntakeCommand;
import frc.robot.commands.ShootCommand;
import frc.robot.commands.TankDriveCommand;
import frc.robot.commands.ToggleTurretTrackingCommand;
import frc.robot.commands.autons.ClawbotPathOne;
import frc.robot.commands.autons.SimpleTwoHighAuto;
import frc.robot.commands.autons.ThreeBallAuto;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LEDs;
import frc.robot.subsystems.Limelight;
import frc.robot.subsystems.Turret;
import frc.robot.util.command.RunEndCommand;
import frc.robot.util.path.RamseteCommandFollower;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    // Autonomous selector which allows us to choose which autonomous command to run
    private final SendableChooser<Command> autonChooser = new SendableChooser<Command>();

    // joysticks
    private final Joystick lJoy = new Joystick(kIO.LEFT_STICK);
    private final Joystick rJoy = new Joystick(kIO.RIGHT_STICK);

    // The robot's subsystems and commands are defined here...
    public final Flywheel flywheel = new Flywheel(); // public so we can update control loops
    public final Turret turret = new Turret(); // public so we can update control loops
    private final Drivetrain drivetrain = new Drivetrain();
    private final Climber climber = new Climber();
    private final Indexer indexer = new Indexer();
    private final Intake intake = new Intake();
    private final Limelight limelight = new Limelight();
    private final LEDs leds = new LEDs();

    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     */
    public RobotContainer() {
        drivetrain.setDefaultCommand(new TankDriveCommand(lJoy::getY, rJoy::getY, rJoy::getThrottle, drivetrain));
        new Trigger(RobotState::isEnabled).whenActive(limelight::setDriving);

        // Adds and sets autonomus actions to the smartdashboard for the user to select
        autonChooser.setDefaultOption("Choose an Autonomous Command", new PrintCommand("Select a command!!"));
        autonChooser.addOption("SHOOT HIGH, GET BALL, SHOOT HIGH",
                new SimpleTwoHighAuto(drivetrain, flywheel, indexer, intake, limelight)
                        .deadlineWith(new ToggleTurretTrackingCommand(turret, limelight, leds)));
        autonChooser.addOption("RUN OFF LINE", new RunEndCommand(() -> {
            drivetrain.drive(0.4, 0.4);
        }, drivetrain::stop, drivetrain).withTimeout(1));
        autonChooser.addOption("SHOOT HIGH AND THEN MOVE",
                new SequentialCommandGroup(
                        new ShootCommand(2200, flywheel, indexer, limelight).withTimeout(3),
                        new RunEndCommand(() -> {
                            drivetrain.drive(-0.2, -0.2);
                        }, drivetrain::stop, drivetrain).withTimeout(2)));
        autonChooser.addOption("THREE BALL", new ThreeBallAuto(drivetrain, flywheel, indexer, intake, limelight));

        autonChooser.addOption("clawbot path one",
                new ClawbotPathOne(drivetrain, intake, indexer, flywheel, limelight));
        // Configure the button bindings
        configureButtonBindings();

        SmartDashboard.putData("Auto Chooser", autonChooser);
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be
     * created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing
     * it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        // left stick ------------------------------------------------------------------

        new JoystickButton(lJoy, 6).toggleWhenPressed(new PlayMusicCommand("birthday.chrp", drivetrain));
        // intake control
        new POVButton(lJoy, 180).whenPressed(new DeployIntakeCommand(intake)); // drop intake on left POV down
        new POVButton(lJoy, 0).whenPressed(new RetractIntakeCommand(intake, indexer)); // stow intake on left POV up
        // left trigger runs intake
        new JoystickButton(lJoy, 1).whileActiveOnce(new RunIntakeCommand(intake, indexer));
        new JoystickButton(lJoy, 2).whileHeld(intake::runReverse, intake).whenReleased(intake::stop, intake);

        // climb
        // TODO: automate this to a single button press which fully extends the climb
        new JoystickButton(lJoy, 3).whileHeld(climber::climb, climber).whenReleased(climber::stop, climber);
        new JoystickButton(lJoy, 4).whileHeld(climber::extend, climber).whenReleased(climber::stop, climber);

        // limelight tuning
        new JoystickButton(lJoy, 6).whenPressed(limelight::takeSnapshot);

        // right stick -----------------------------------------------------------------

        // shooting
        // trigger shoots into the low goal
        new JoystickButton(rJoy, 1).whileActiveOnce(new ShootCommand(2100, flywheel, indexer, limelight));
        // some random button shoots high
        new JoystickButton(rJoy, 6).whileActiveOnce(new ShootCommand(1500, flywheel, indexer, limelight));

        new JoystickButton(rJoy, 4).whenPressed(drivetrain::dropAnchor);

        new JoystickButton(rJoy, 3).whenPressed(new RamseteCommandFollower(() -> {
            ArrayList<Translation2d> interiorWaypoints = new ArrayList<Translation2d>();
            Pose2d curretPose = drivetrain.getPose();
            Pose2d anchor = drivetrain.getAnchor();

            try {
                if (!drivetrain.isNearWaypoint(anchor, 0.5)) {

                    Trajectory trajectory = TrajectoryGenerator.generateTrajectory(curretPose, interiorWaypoints,
                            anchor,
                            Constants.kDrivetrain.CONFIG);

                    // might need to remove dual trajectory creation due to performance issues idk
                    Trajectory revtrajectory = TrajectoryGenerator.generateTrajectory(curretPose, interiorWaypoints,
                            anchor,
                            Constants.kDrivetrain.REVERSE_CONFIG);
                    trajectory = trajectory.getTotalTimeSeconds() > revtrajectory.getTotalTimeSeconds() ? revtrajectory
                            : trajectory;
                    return trajectory;
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }, drivetrain)).whenInactive(drivetrain::stop, drivetrain);

        // turret control
        new POVButton(rJoy, 0).whenPressed(() -> turret.setTurretTargetDegrees(0), turret); // up is full forward
        new POVButton(rJoy, 180).toggleWhenActive(new ToggleTurretTrackingCommand(turret, limelight, leds)); // dwn auto
        new POVButton(rJoy, 90).whenPressed(() -> turret.setTurretTargetDegrees(35), turret); // left is full left
        new POVButton(rJoy, 270).whenPressed(() -> turret.setTurretTargetDegrees(-35), turret); // right is full right
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        // An ExampleCommand will run in autonomous
        return autonChooser.getSelected();
    }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TankDriveCommand extends CommandBase {
    private final Drivetrain drivetrain;
    private final DoubleSupplier left, right, throt;

    /**
     * Creates a new TankDriveCommand. This will drive the drivetrain based off the
     * left and right data source. The speed of both sides is controlled by the
     * throttle.
     * <p>
     * Drives like a tank where the left data source controls the left side of the
     * robot and right data source controls the right side of the robot.
     * <p>
     * The data sources are already inverted such that a positive number drives
     * backwards and a negative number drives forward (as a joystick would return).
     * 
     * @param left       Left data input source.
     * @param right      Right data input source.
     * @param throt      Throttle data input source.
     * @param drivetrain Drivetrain to drive.
     */
    public TankDriveCommand(DoubleSupplier left, DoubleSupplier right, DoubleSupplier throt, Drivetrain drivetrain) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.drivetrain = drivetrain;
        this.left = left;
        this.right = right;
        this.throt = throt;
        addRequirements(drivetrain);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        double throttle = (1.0 - throt.getAsDouble()) / -2.0;
        drivetrain.drive(left.getAsDouble() * throttle, right.getAsDouble() * throttle);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        drivetrain.stop();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}

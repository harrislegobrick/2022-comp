// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Limelight;
import frc.robot.util.command.RunEndCommand;

public class ShootCommand extends ParallelCommandGroup {
    private final double kDefaultRPM;
    // private final Limelight limelight;

    /**
     * Creates a new ShootCommand. This will run the flywheel based off of the
     * distance calculation from the vision camera, and then once the flywheel is up
     * to speed will run the indexer.
     * 
     * @param kDefaultRPM The RPM to set the motor to if the vision does not detect
     *                    a target
     * @param flywheel    The robot's flywheel.
     * @param indexer     The robot's indexer.
     * @param limelight   The robot's vision camera.
     */
    public ShootCommand(double kDefaultRPM, Flywheel flywheel, Indexer indexer, Limelight limelight) {
        // this.limelight = limelight;
        this.kDefaultRPM = kDefaultRPM;
        addCommands(new RunEndCommand(() -> flywheel.setRPM(calculateRPM()), flywheel::stop, flywheel),
                new RunEndCommand(indexer::reverse, indexer::stop, indexer).withTimeout(0.35).andThen(
                new RunEndCommand(() -> {
                    if (Units.radiansPerSecondToRotationsPerMinute(
                            flywheel.getFilteredRate()) > (calculateRPM() * 0.97)) {
                        indexer.run();
                    } else {
                        indexer.stop();
                    }
                }, indexer::stop, indexer))
        // new WaitUntilCommand(
        // () -> Units.radiansPerSecondToRotationsPerMinute(flywheel.getFilteredRate())
        // > (calculateRPM() * 0.98))
        // .andThen(new RunEndCommand(indexer::runSlow, indexer::stop)));
        );
    }

    /**
     * Calculates the optimal RPM to set the motor to by interpolating between
     * predetermined distance-RPM pairs.
     * 
     * @return Calculated RPM that the motor should be set to based on the distance.
     *         If the camera cannot determine the distance, it will just return
     *         {@code kDefaultRPM}.
     */
    private double calculateRPM() {
        // implement this when the values for distance-RPM have been tested
        // if (limelight.hasTarget()) {
        // return kFlywheel.SHOT_TABLE.get(limelight.getHUBDistance());
        // } else {
        // return kDefaultRPM;
        // }

        // temp fix
        return kDefaultRPM;
    }
}

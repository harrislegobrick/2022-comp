// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.Intake;

public class DeployIntakeCommand extends SequentialCommandGroup {
  /**
   * Sequency to deploy the intake. It extends the intake and then sets the intake
   * to not be applying pneumatic pressure to the piston.
   */
  public DeployIntakeCommand(Intake intake) {
    addCommands(
        new InstantCommand(intake::deploy),
        new WaitCommand(0.5),
        new InstantCommand(intake::limp)

    );
  }
}

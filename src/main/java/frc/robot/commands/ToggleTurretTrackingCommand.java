// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LEDs;
import frc.robot.subsystems.Turret;
import frc.robot.subsystems.Limelight;

public class ToggleTurretTrackingCommand extends CommandBase {
    private final Turret turret;
    private final Limelight limelight;
    private final LEDs leds;
    private final double kEpsilon = 3;

    /**
     * Creates a new ToggleTurretTrackingCommand. When the command starts, the
     * camera's LEDs will turn on and the camera will turn the turret based off the
     * camera's readings. When the command ends, the LEDs on the robot and the
     * vision camera will turn off. While the command is active, the turret will try
     * to track the target such that the turret is facing the target (the target
     * being the UPPER HUB).
     * <p>
     * When the target is "locked" (or within some threshold of being accurately
     * tracked), the LEDs on the robot will turn on to visually indicate to the
     * driver that the robot's turret is alligned.
     * 
     * @param turret       The robot's turret.
     * @param visionCamera The robot's vision camera.
     * @param leds         The robot's LEDs.
     */
    public ToggleTurretTrackingCommand(Turret turret, Limelight visionCamera, LEDs leds) {
        this.turret = turret;
        this.limelight = visionCamera;
        this.leds = leds;
        addRequirements(turret, visionCamera, leds);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        limelight.setTracking();
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        if (limelight.hasTarget()) {
            // figure out what the curret reading of the limelight's error is
            double captureError = limelight.getX();

            if (captureError < kEpsilon && captureError > -kEpsilon) {
                leds.turnOn();
            } else {
                // find how long it took to capture and process the image
                double captureLatency_s = limelight.getLatency();
                // current time in seconds (divide by 1 million to convert microseconds to
                // seconds)
                double currentTime_s = RobotController.getFPGATime() / 1e6;
                // find when the image was taken
                double captureTime_s = currentTime_s - captureLatency_s;

                // find what the turrent angle was at the time the image was taken
                double capturePose = turret.getPoseAtTime(captureTime_s);
                // update the wanted turret angle based on where the turret was when the current
                // vision measurement was taken
                double wantedPose = capturePose + captureError;

                turret.setTurretTargetDegrees(wantedPose);
                leds.turnOff();
            }
        } else {
            // if we don't see a target, turn off the LEDs
            leds.turnOff();
        }
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        limelight.setDriving();
        leds.turnOff();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}

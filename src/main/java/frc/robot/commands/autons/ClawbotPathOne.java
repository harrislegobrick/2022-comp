// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autons;

import java.util.List;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants.kDrivetrain;
import frc.robot.commands.RetractIntakeCommand;
import frc.robot.commands.RunIntakeCommand;
import frc.robot.commands.ShootCommand;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Limelight;
import frc.robot.util.path.RamseteCommandFollower;

public class ClawbotPathOne extends SequentialCommandGroup {
  /** Creates a new ClawbotPathOne. */
  public ClawbotPathOne(Drivetrain drivetrain, Intake intake, Indexer indexer, Flywheel flywheel, Limelight limelight) {
    Trajectory trajectory1 = TrajectoryGenerator.generateTrajectory(new Pose2d(0, 0, Rotation2d.fromDegrees(270)),
        List.of(),
        new Pose2d(0, Units.feetToMeters(4), Rotation2d.fromDegrees(270)), kDrivetrain.REVERSE_CONFIG);
    Trajectory trajectory2 = TrajectoryGenerator.generateTrajectory(new Pose2d(0, Units.feetToMeters(4), Rotation2d.fromDegrees(270)),
        List.of(
            new Translation2d(Units.feetToMeters(7), Units.feetToMeters(4)),
            new Translation2d(Units.feetToMeters(7), Units.feetToMeters(9))),
        new Pose2d(Units.feetToMeters(13), Units.feetToMeters(9), Rotation2d.fromDegrees(0)),
        kDrivetrain.REVERSE_CONFIG);

    addCommands(
        new InstantCommand(intake::deploy, intake),
        new RamseteCommandFollower(trajectory1, drivetrain).deadlineWith(new RunIntakeCommand(intake, indexer)),
        new RetractIntakeCommand(intake, indexer),
        new RamseteCommandFollower(trajectory2, drivetrain),
        new ShootCommand(1500, flywheel, indexer, limelight).withTimeout(3)

    );
  }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autons;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.DeployIntakeCommand;
import frc.robot.commands.RunIntakeCommand;
import frc.robot.commands.ShootCommand;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Limelight;
import frc.robot.util.command.RunEndCommand;

public class SimpleTwoHighAuto extends SequentialCommandGroup {
    /** Creates a new SimpleTwoBallAuto. */
    public SimpleTwoHighAuto(Drivetrain drivetrain, Flywheel flywheel, Indexer indexer, Intake intake,
            Limelight limelight) {
        addCommands(

                // new ShootCommand(2050, flywheel, indexer, limelight).withTimeout(3),
                new DeployIntakeCommand(intake),
                new WaitCommand(0.5),
                new RunEndCommand(() -> {
                    drivetrain.drive(-0.2, -0.2);
                }, drivetrain::stop, drivetrain)
                        .deadlineWith(new RunIntakeCommand(intake, indexer))
                        .withTimeout(2),
                new InstantCommand(intake::stow, intake),
                new RunEndCommand(() -> {
                    drivetrain.drive(0.2, 0.2);
                }, drivetrain::stop, drivetrain).withTimeout(0.75),
                new ShootCommand(2100, flywheel, indexer, limelight)
                        .withTimeout(6)

        );
    }
}

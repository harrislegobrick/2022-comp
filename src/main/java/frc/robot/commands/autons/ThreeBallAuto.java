// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autons;

import java.util.List;

import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.constraint.CentripetalAccelerationConstraint;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants.kDrivetrain;
import frc.robot.commands.DeployIntakeCommand;
import frc.robot.commands.RunIntakeCommand;
import frc.robot.commands.ShootCommand;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Limelight;
import frc.robot.util.path.RamseteCommandFollower;
import frc.robot.util.path.TrajectoryHelper;

public class ThreeBallAuto extends SequentialCommandGroup {
  private final double maxAcceleration = Units.feetToMeters(2);
  private final double maxVelocity = Units.feetToMeters(10);
  private final double maxCentripitalAcceleration = Units.feetToMeters(2);
  private final double endVelocity = Units.feetToMeters(0);
  /** Creates a new ThreeBallAuto. */
  public ThreeBallAuto(Drivetrain drivetrain, Flywheel flywheel, Indexer indexer, Intake intake, Limelight limelight) {
    TrajectoryConfig forwardsConfig = new TrajectoryConfig(maxVelocity, maxAcceleration);
    forwardsConfig.setKinematics(kDrivetrain.DRIVE_KINEMATICS);
    forwardsConfig.setEndVelocity(endVelocity);
    forwardsConfig.addConstraints(List.of(kDrivetrain.autoVoltageConstraint,
        new CentripetalAccelerationConstraint(maxCentripitalAcceleration)));

    TrajectoryConfig backwardsConfig = new TrajectoryConfig(maxVelocity, maxAcceleration);
    backwardsConfig.setKinematics(kDrivetrain.DRIVE_KINEMATICS);
    backwardsConfig.setEndVelocity(endVelocity);
    backwardsConfig.setReversed(true);
    backwardsConfig.addConstraints(List.of(kDrivetrain.autoVoltageConstraint,
        new CentripetalAccelerationConstraint(maxCentripitalAcceleration)));

    Trajectory trajectory1 = TrajectoryHelper.generateTrajectory("ThreePathOne", backwardsConfig);
    Trajectory trajectory2 = TrajectoryHelper.generateTrajectory("ThreePathTwo", forwardsConfig);
    Trajectory trajectory3 = TrajectoryHelper.generateTrajectory("ThreePathThree", backwardsConfig);
    Trajectory trajectory4 = TrajectoryHelper.generateTrajectory("ThreePathFour", forwardsConfig);

    RamseteCommandFollower run1 = new RamseteCommandFollower(trajectory1, drivetrain);
    RamseteCommandFollower run2 = new RamseteCommandFollower(trajectory2, drivetrain).startAtTrajectoryStart(false);
    RamseteCommandFollower run3 = new RamseteCommandFollower(trajectory3, drivetrain).startAtTrajectoryStart(false);
    RamseteCommandFollower run4 = new RamseteCommandFollower(trajectory4, drivetrain).startAtTrajectoryStart(false);
    
    addCommands(
        new ShootCommand(2200, flywheel, indexer, limelight).withTimeout(2),
        new DeployIntakeCommand(intake),
        new WaitCommand(0.5),
        run1.deadlineWith(new RunIntakeCommand(intake, indexer)),
        new InstantCommand(intake::stow, intake),
        run2,
        new ShootCommand(2200, flywheel, indexer, limelight).withTimeout(2),
        new DeployIntakeCommand(intake),
        new WaitCommand(0.5),
        run3.deadlineWith(new RunIntakeCommand(intake, indexer)),
        new InstantCommand(intake::stow, intake),
        run4,
        new ShootCommand(2200, flywheel, indexer, limelight).withTimeout(2)

    );
  }
}

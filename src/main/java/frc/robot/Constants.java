// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrapezoidProfile.Constraints;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.math.util.Units;
import frc.robot.util.interpolation.InterpolatingTreeMap;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean
 * constants. This class should not be used for any other purpose. All constants
 * should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    public static final class kIO {
        public static final int LEFT_STICK = 0;
        public static final int RIGHT_STICK = 1;
    }

    public static final class kVision {
        // HUB limelight constants for distance
        public static final double LIMELIGHT_HEIGHT = Units.inchesToMeters(40);
        public static final double HUB_TARGET_HEIGHT_METERS = Units.inchesToMeters(102);// Units.inchesToMeters(8.0 *
                                                                                        // 12.0 + 8.0); // 8 ft 8 in
        // measured from the horizontal axis
        public static final double HUB_CAMERA_PITCH_RADIANS = Units.degreesToRadians(28.5);
    }

    public static final class kDrivetrain {
        public static final double WHEEL_DIAMETER = Units.inchesToMeters(6);
        public static final double WHEEL_CIRCUMFERENCE = WHEEL_DIAMETER * Math.PI;
        public static final int ENCODER_CPR = 2048; // SRX mag encoder is 4096, Falcon 500 is 2048

        // this number would be 1 if the encoder was connected directly to the output
        // shaft, but since the encoder is on the input shaft, we have to multiply by
        // the gear ratio of the kitframe
        public static final double GEAR_RATIO = 10.71;

        /**
         * Converts the native unit of the motor's encoder (CPR) to meters.
         */
        public static final double TICKS_TO_METERS = (1.0 / ENCODER_CPR) * (1.0 / GEAR_RATIO) * WHEEL_CIRCUMFERENCE;

        public static final double TRACK_WIDTH_METERS = 1.2529241489626137; // TODO: characterize for actual value

        public static final DifferentialDriveKinematics DRIVE_KINEMATICS = new DifferentialDriveKinematics(
                TRACK_WIDTH_METERS);

        public static final int FRONT_LEFT = 3;
        public static final int BACK_LEFT = 4;
        public static final int FRONT_RIGHT = 1;
        public static final int BACK_RIGHT = 2;

        public static final boolean GYRO_INVERTED = true;
        public static final boolean DRIVE_INVERTED = false;
        public static final int ENCODER_WINDOW_SIZE = 4;
        public static final int MOTOR_TIMEOUT = 10;
        public static final int PID_SLOT = 0;

        public static final SupplyCurrentLimitConfiguration CURRENT_LIMIT_CONFIG = new SupplyCurrentLimitConfiguration(
                // enabled | Limit(amp) | Trigger Threshold(amp) | Trigger Threshold Time(s)
                true, 50, 55, 0.5);

        // TODO: will need characterization to find values
        public static final double S_VOLTS = 0.62939;
        public static final double V_VOLTS_SECOND_PER_METER = 2.3795;
        public static final double A_VOLT_SEONDS_SQUARED_PER_METER = 0.39125;

        public static final double MAX_SPEED_METERS_PER_SECOND = Units.feetToMeters(4.0);
        public static final double MAX_ACCELERATION_METERS_PER_SECOND_SQUARED = Units.feetToMeters(3.0);
        public static final double P_DRIVE_VEL = 3.3416;

        public static final double MAX_VOLTAGE = 11;
        public static final DifferentialDriveVoltageConstraint autoVoltageConstraint = new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(S_VOLTS, V_VOLTS_SECOND_PER_METER, A_VOLT_SEONDS_SQUARED_PER_METER),
                DRIVE_KINEMATICS, MAX_VOLTAGE);
        public static final TrajectoryConfig CONFIG = new TrajectoryConfig(MAX_SPEED_METERS_PER_SECOND,
                MAX_ACCELERATION_METERS_PER_SECOND_SQUARED).setKinematics(DRIVE_KINEMATICS)
                        .addConstraint(autoVoltageConstraint);
        public static final TrajectoryConfig REVERSE_CONFIG = new TrajectoryConfig(MAX_SPEED_METERS_PER_SECOND,
                MAX_ACCELERATION_METERS_PER_SECOND_SQUARED).setKinematics(DRIVE_KINEMATICS)
                        .addConstraint(autoVoltageConstraint).setReversed(true);

    }

    public static final class kFlywheel {
        public static final int LEFT_MOTOR = 5;
        public static final int RIGHT_MOTOR = 6;
        public static final boolean INVERTED = true;

        public static final int[] ENCODER_PORT = { 0, 1 };
        public static final boolean ENCODER_INVERTED = true;
        public static final double ENCODER_CPR = 2048.0;
        // We go 2 pi radians per encoder cpr (^)
        public static final double ENCODER_CONVERSION = (2.0 * Math.PI) / ENCODER_CPR;

        // tuned to flywheel on 3/7/22
        public static final double kV = 0.02078; // 0.0198;
        public static final double kA = 0.005291; // 0.002407;
        public static final double CONTROL_LOOP_PERIOD = 0.01;

        public static final InterpolatingTreeMap<Double, Integer> SHOT_TABLE = new InterpolatingTreeMap<Double, Integer>();
        static {
            // SHOT_TABLE.put(distance, RPM)
            // 6 ft 10 in
            SHOT_TABLE.put(Units.inchesToMeters(6 * 12 + 10), 2000);
            // 10 ft 4 in
            SHOT_TABLE.put(Units.inchesToMeters(10 * 12 + 4), 2500);
            // 13 ft 6 in
            SHOT_TABLE.put(Units.inchesToMeters(13 * 12 + 6), 3000);
            // 17 ft 6 in
            SHOT_TABLE.put(Units.inchesToMeters(17 * 12 + 6), 3500);

        }
    }

    public static final class kIntake {
        public static final int LEFT_MOTOR = 7;
        public static final double SPEED = 0.85; // percentage
        public static final boolean REVERSED = true;

        public static final int[] PISTONS = { 7, 5 };
    }

    public static final class kTurret {
        public static final int MOTOR = 8;
        public static final boolean INVERTED = false;
        public static final double kS = 0.2482;
        public static final double kV = 2.8219;
        public static final double kA = 0.2;//0.18917;
        public static final double CONTROL_LOOP_PERIOD = 0.01;
        public static final double REDUCTION = 140; // 10:1 gearbox and 140:10 ratio for the turret gears

        // mass is about 9 lbs which is around 4kg, distance is about 10.75 in which is
        // about 0.27305 meters
        // using the equation J = 1/3 m l^2, J = 1/3 * 4 * (0.27305)^2
        public static final double kJ = 0.11272714121;
        public static final Constraints SSConstraints = new Constraints(Units.degreesToRadians(360),
                Units.degreesToRadians(360)); // max speed and acceleration
        public static final int MIN_ROTATE_DEG = -45;
        public static final int MAX_ROTATE_DEG = 45;

    }

    public static final class kIndexer {
        public static final int IN_MOTOR = 9;
        public static final int BACK_MOTOR = 10;
        public static final boolean INVERTED = false;
        public static final double SPEED = 0.35;
        public static final int TIMEOUT = 10;
        public static final int BOTTOM_SENSOR = 2;
        public static final int TOP_SENSOR = 3;
    }

    public static final class kClimber {
        public static final int LEFT_MOTOR = 11;
        public static final boolean INVERTED = false;
        public static final double SPEED = 0.6;

        public static final int[] PISTONS = { 5, 8 };
        public static final int RIGHT_MOTOR = 12;
    }
}

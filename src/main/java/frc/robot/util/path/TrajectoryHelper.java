// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.util.path;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

import edu.wpi.first.math.spline.Spline.ControlVector;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrajectoryGenerator.ControlVectorList;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;

/**
 * Class containing methods to help with generating trajectories from Pathweaver
 * paths in the deploy/paths directory.
 */
public class TrajectoryHelper {
    /**
     * Number obtained through placing a waypoint at (0, 0) in PathWeaver and
     * finding out how low down it is.
     */
    private static final double PATHWEAVER_Y_OFFSET = 8.2296;

    // Helper class with static methods so we don't need to ever make an object out
    // of it.
    private TrajectoryHelper() {
    }

    /**
     * Get control vector list from path file.
     * 
     * @param pathName Specify the {THIS} in src/main/deploy/waypoints/{THIS}.path
     * @return control vectors from file
     * @throws FileNotFoundException
     */
    public static ControlVectorList getControlVectors(String pathName) {
        String relativePath = "waypoints/" + pathName;
        Path path = Filesystem.getDeployDirectory().toPath().resolve(relativePath);

        ControlVectorList controlVectors = new ControlVectorList();

        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
            reader.lines().skip(1).forEachOrdered(line -> {
                String[] split = line.split(",");
                controlVectors.add(new ControlVector(
                        new double[] { Double.parseDouble(split[0]), Double.parseDouble(split[2]), 0 }, new double[] {
                                Double.parseDouble(split[1]) + PATHWEAVER_Y_OFFSET, Double.parseDouble(split[3]), 0 }));
            });
        } catch (IOException e) {
            DriverStation.reportError(
                    String.format("%nFailed to generate custom trajectory for path name %s!!%n%n", pathName), false);
        }

        return controlVectors;
    }

    /**
     * Generates a trajectory from the control points located in the waypoints
     * folder with a user defined trajectory configuration. This allows for more
     * control than simply exporting a trajectory from PathWeaver.
     * 
     * @param pathName File name in the waypoints folder.
     * @param config   A trajectory config for the path to use.
     * @return A trajectory (or null if it cannot generate a trajectory because of
     *         an incorrect file name).
     */
    public static Trajectory generateTrajectory(String pathName, TrajectoryConfig config) {
        return TrajectoryGenerator.generateTrajectory(getControlVectors(pathName), config);
    }
}

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.util.command;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.Subsystem;

import static edu.wpi.first.wpilibj.util.ErrorMessages.requireNonNullParam;

/**
 * This will run the first provided runnable constantly and then call the second
 * runnable when the command is intterupted.
 */
public class RunEndCommand extends CommandBase {
    private final Runnable executeRunnable;
    private final Runnable endRunnable;

    /**
     * Creates a new RunEndCommand. Will run the first runnables perpetually and
     * then run the second runnable when this command is interrupted.
     *
     * @param executeRunnable the Runnable to perpetually run
     * @param onEnd           the Runnable to run on command end
     * @param requirements    the subsystems required by this command
     */
    public RunEndCommand(Runnable executeRunnable, Runnable endRunnable, Subsystem... subsystems) {
        this.executeRunnable = requireNonNullParam(executeRunnable, "executeRunnable", "RunEndCommand");
        this.endRunnable = requireNonNullParam(endRunnable, "endRunnable", "RunEndCommand");

        addRequirements(subsystems);
    }

    @Override
    public void execute() {
        executeRunnable.run();
    }

    @Override
    public void end(boolean interrupted) {
        endRunnable.run();
    }
}

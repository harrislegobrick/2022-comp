// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.ArrayList;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.music.Orchestra;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kDrivetrain;

/** Creates a new Drivetrain. */
public class Drivetrain extends SubsystemBase {
    // Declare objects
    private final WPI_TalonFX leftCommander, rightCommander, leftFollower, rightFollower;
    private final DifferentialDriveOdometry odometry;
    private final Orchestra orchestra;
    private final Field2d field;
    private final AHRS gyro;
    private Pose2d anchor;

    public Drivetrain() {
        // initalize objects
        leftCommander = new WPI_TalonFX(kDrivetrain.FRONT_LEFT);
        leftFollower = new WPI_TalonFX(kDrivetrain.BACK_LEFT);
        rightCommander = new WPI_TalonFX(kDrivetrain.FRONT_RIGHT);
        rightFollower = new WPI_TalonFX(kDrivetrain.BACK_RIGHT);
        field = new Field2d();

        gyro = new AHRS();

        odometry = new DifferentialDriveOdometry(getHeading());

        ArrayList<TalonFX> instruments = new ArrayList<>();
        instruments.add(leftCommander);
        instruments.add(rightCommander);
        instruments.add(leftFollower);
        instruments.add(rightFollower);
        orchestra = new Orchestra(instruments);

        // reset talons
        leftCommander.configFactoryDefault(kDrivetrain.MOTOR_TIMEOUT);
        leftFollower.configFactoryDefault(kDrivetrain.MOTOR_TIMEOUT);
        rightCommander.configFactoryDefault(kDrivetrain.MOTOR_TIMEOUT);
        rightFollower.configFactoryDefault(kDrivetrain.MOTOR_TIMEOUT);

        // followers follow their commanders
        leftFollower.follow(leftCommander);
        rightFollower.follow(rightCommander);

        // set motors to brake
        leftCommander.setNeutralMode(NeutralMode.Brake);
        leftFollower.setNeutralMode(NeutralMode.Brake);
        rightCommander.setNeutralMode(NeutralMode.Brake);
        rightFollower.setNeutralMode(NeutralMode.Brake);

        /**
         * Configure the current limits that will be used
         * Supply Current is the current that passes into the controller from the supply
         * Use supply current limits to prevent breakers from tripping
         */
        rightCommander.configSupplyCurrentLimit(kDrivetrain.CURRENT_LIMIT_CONFIG, kDrivetrain.MOTOR_TIMEOUT);
        leftCommander.configSupplyCurrentLimit(kDrivetrain.CURRENT_LIMIT_CONFIG, kDrivetrain.MOTOR_TIMEOUT);
        rightFollower.configSupplyCurrentLimit(kDrivetrain.CURRENT_LIMIT_CONFIG, kDrivetrain.MOTOR_TIMEOUT);
        leftFollower.configSupplyCurrentLimit(kDrivetrain.CURRENT_LIMIT_CONFIG, kDrivetrain.MOTOR_TIMEOUT);

        // invert the motors so that positive is forward
        leftCommander.setInverted(kDrivetrain.DRIVE_INVERTED);
        rightCommander.setInverted(!kDrivetrain.DRIVE_INVERTED);
        leftFollower.setInverted(InvertType.FollowMaster);
        rightFollower.setInverted(InvertType.FollowMaster);

        // encoder setup
        leftCommander.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,
                kDrivetrain.PID_SLOT,
                kDrivetrain.MOTOR_TIMEOUT);
        rightCommander.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,
                kDrivetrain.PID_SLOT,
                kDrivetrain.MOTOR_TIMEOUT);

        // set encoders to have less measurement lag
        // Allows for more time relavant velocity measurements at the cost of a litte
        // more noise
        leftCommander.configVelocityMeasurementWindow(kDrivetrain.ENCODER_WINDOW_SIZE, kDrivetrain.MOTOR_TIMEOUT);
        rightCommander.configVelocityMeasurementWindow(kDrivetrain.ENCODER_WINDOW_SIZE, kDrivetrain.MOTOR_TIMEOUT);

        // CAN bus optimization
        leftFollower.setStatusFramePeriod(1, 255, kDrivetrain.MOTOR_TIMEOUT);
        leftFollower.setStatusFramePeriod(2, 255, kDrivetrain.MOTOR_TIMEOUT);
        rightFollower.setStatusFramePeriod(1, 255, kDrivetrain.MOTOR_TIMEOUT);
        rightFollower.setStatusFramePeriod(2, 255, kDrivetrain.MOTOR_TIMEOUT);

        // zero sensors
        resetEncoders();
        resetGyro();

        // put stuff on the dashboard so we can see it
        SmartDashboard.putData(gyro);
        SmartDashboard.putData(field);
        anchor = new Pose2d();
    }

    public void playMusic(String song) {
        orchestra.loadMusic(song);
        orchestra.play();
    }

    public void stopMusic() {
        orchestra.stop();
    }

    /**
     * Changes where the robot should drive to.
     */
    public void dropAnchor() {
        anchor = getPose();
    }

    /**
     * Gets the position where the robot should drive to.
     * 
     * @return The Pose2d where the robot should go to.
     */
    public Pose2d getAnchor() {
        return anchor;
    }

    /**
     * Returns the current wheel speeds of the robot.
     *
     * @return The current wheel speeds as a {@link DifferentialDriveWheelSpeeds}.
     */
    public DifferentialDriveWheelSpeeds getWheelSpeeds() {
        return new DifferentialDriveWheelSpeeds(
                leftCommander.getSelectedSensorVelocity() * kDrivetrain.TICKS_TO_METERS * 10.0,
                rightCommander.getSelectedSensorVelocity() * kDrivetrain.TICKS_TO_METERS * 10.0);
        // multiply by 10 because velocity from encoder is given in ms and we want s
    }

    /**
     * Resets the encoders on the drivebase. Usually called when resetting the
     * odometry.
     */
    public void resetEncoders() {
        leftCommander.setSelectedSensorPosition(0.0);
        rightCommander.setSelectedSensorPosition(0.0);
    }

    /**
     * Drives with percentage. Usually used with joystick input or any other input
     * ranging from -1 to 1. Goes off of percentage and not voltage.
     * 
     * @param left  percent to drive the left side.
     * @param right percent to drive the right side.
     */
    public void drive(double left, double right) {
        leftCommander.set(ControlMode.PercentOutput, left);
        rightCommander.set(ControlMode.PercentOutput, right);
    }

    /**
     * Drives with voltage. Usually used with feedforward or more advanced autons.
     * 
     * @param leftVolts  voltage for the left side.
     * @param rightVolts voltage for the right side.
     */
    public void driveVolts(double leftVolts, double rightVolts) {
        leftCommander.setVoltage(leftVolts);
        rightCommander.setVoltage(rightVolts);
    }

    /**
     * Resets the orientation of the gyro.
     */
    public void resetGyro() {
        gyro.zeroYaw();
    }

    /**
     * Commands both drive motors to 0, stopping the drivetrain.
     */
    public void stop() {
        rightCommander.set(ControlMode.PercentOutput, 0);
        leftCommander.set(ControlMode.PercentOutput, 0);
    }

    /**
     * Returns the heading of the robot as obtained through the NavX gyro.
     *
     * @return The robot's heading as a {@link Rotation2d}.
     */
    public Rotation2d getHeading() {
        return Rotation2d
                .fromDegrees(Math.IEEEremainder(gyro.getAngle(), 360) * (kDrivetrain.GYRO_INVERTED ? -1.0 : 1.0));
    }

    /**
     * Gets the robot's position as obtained through odometry with the encoders and
     * gyro.
     * 
     * @return The robot's position as a {@link Pose2d}.
     */
    public Pose2d getPose() {
        return odometry.getPoseMeters();
    }

    /**
     * Checks to see if the drivetrain is close to a waypoint. Useful for activating
     * commands that require the drivetrain be at a specific position in a path.
     * 
     * @param waypoint The point on the field that the robot must be close to.
     * @param within   The distance the robot must be from the waypoint to return
     *                 true. This should be sufficiently large enough to account for
     *                 robot inaccuracies when following a path.
     * @return {@code true} if the robot is close to the waypoint, {@code false} if
     *         the robot is not near the waypoint.
     */
    public boolean isNearWaypoint(Pose2d waypoint, double within) {
        Pose2d currentPos = getPose();
        double distance = currentPos.getTranslation().getDistance(waypoint.getTranslation());
        return distance <= within;
    }

    /**
     * Sets the position of the robot on the field. Usually called at the start of
     * auton to initally position the robot within the field.
     * 
     * @param poseMeters a {@link Pose2d} representing the robot's position on the
     *                   field.
     */
    public void setPosition(Pose2d poseMeters) {
        resetEncoders();
        odometry.resetPosition(poseMeters, getHeading());
    }

    /**
     * Useful for drawing other objects to the field.
     * 
     * @return The {@link Field2d} object.
     */
    public Field2d getField2d() {
        return field;
    }

    @Override
    public void periodic() {
        if (RobotState.isEnabled()) {
            // update where the robot thinks it is
            odometry.update(getHeading(),
                    leftCommander.getSelectedSensorPosition() * kDrivetrain.TICKS_TO_METERS,
                    rightCommander.getSelectedSensorPosition() * kDrivetrain.TICKS_TO_METERS);
            // draw where the robot thinks it is onto the virtual field
            field.setRobotPose(getPose());
        }
    }
}

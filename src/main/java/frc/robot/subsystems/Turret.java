// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMaxLowLevel.PeriodicFrame;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.Nat;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.controller.LinearQuadraticRegulator;
import edu.wpi.first.math.estimator.KalmanFilter;
import edu.wpi.first.math.interpolation.TimeInterpolatableBuffer;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N2;
import edu.wpi.first.math.system.LinearSystem;
import edu.wpi.first.math.system.LinearSystemLoop;
import edu.wpi.first.math.system.plant.LinearSystemId;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kTurret;

public class Turret extends SubsystemBase {
    private final CANSparkMax motor;
    private final RelativeEncoder encoder;
    private final TimeInterpolatableBuffer<Double> buffer;
    private double turretTargetDeg = 0.0;

    private final LinearSystem<N2, N1, N1> turretPlant = LinearSystemId.identifyPositionSystem(kTurret.kV, kTurret.kA);

    private final KalmanFilter<N2, N1, N1> turretObserver = new KalmanFilter<>(
            Nat.N2(), Nat.N1(),
            turretPlant, VecBuilder.fill(0.03, 0.3), // How accurate we think our model is
            VecBuilder.fill(0.01), // How nice we think our encoder data is
            kTurret.CONTROL_LOOP_PERIOD);

    private final LinearQuadraticRegulator<N2, N1, N1> turretController = new LinearQuadraticRegulator<>(
            turretPlant,
            VecBuilder.fill(Units.degreesToRadians(0.1), Units.degreesToRadians(10)),
            // qelms. Velocity error tolerance, in radians per second. Decrease
            // this to more heavily penalize state excursion, or make the controller behave
            // more aggressively.

            VecBuilder.fill(10), // relms. Control effort (voltage) tolerance. Decrease this to more
            // heavily penalize control effort, or make the controller less aggressive. 12
            // is a good
            // starting point because that is the (approximate) maximum voltage of a
            // battery.
            kTurret.CONTROL_LOOP_PERIOD); // Nominal time between loops. 0.020 for TimedRobot, but can be lower if using
                                          // notifiers.

    private final LinearSystemLoop<N2, N1, N1> turretLoop = new LinearSystemLoop<>(
            turretPlant,
            turretController,
            turretObserver,
            12.0,
            kTurret.CONTROL_LOOP_PERIOD);

    private TrapezoidProfile.State lastProfiledReference = new TrapezoidProfile.State();

    /** Creates a new Turret. */
    public Turret() {
        buffer = TimeInterpolatableBuffer.createDoubleBuffer(0.5);

        motor = new CANSparkMax(kTurret.MOTOR, MotorType.kBrushless);
        encoder = motor.getEncoder();

        motor.restoreFactoryDefaults();

        // this will make the encoder read degrees of turret
        encoder.setPositionConversionFactor(360.0 / kTurret.REDUCTION);

        // init default starting position of zero
        encoder.setPosition(0.0);

        motor.setInverted(kTurret.INVERTED);
        motor.setSmartCurrentLimit(40);
        motor.setIdleMode(IdleMode.kCoast);
        motor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, (int) (kTurret.CONTROL_LOOP_PERIOD * 1000.0));
        motor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, (int) (kTurret.CONTROL_LOOP_PERIOD * 1000.0));
        motor.burnFlash();

        // Adjust our LQR's controller for 25 ms of sensor input delay. We
        // provide the linear system, discretization timestep, and the sensor
        // input delay as arguments.
        // turretController.latencyCompensate(turretPlant, kTurret.CONTROL_LOOP_PERIOD,
        // 0.025);

        turretLoop.reset(VecBuilder.fill(0, 0));
        lastProfiledReference = new TrapezoidProfile.State(0, 0);
        
        // May be needed idk
        // Adjust our LQR's controller for 25 ms of sensor input delay. We
        // provide the linear system, discretization timestep, and the sensor
        // input delay as arguments.
        // turretController.latencyCompensate(turretPlant, kTurret.CONTROL_LOOP_PERIOD,
        //         0.0028);

    }

    public double getRateRPM() {
        return encoder.getVelocity();
    }

    public double getAngleDegrees() {
        return encoder.getPosition();
    }

    public void run() {
        motor.set(0.3);
    }

    public void setTurretTargetDegrees(double degrees) {
        if (degrees < kTurret.MIN_ROTATE_DEG) {
            turretTargetDeg = kTurret.MIN_ROTATE_DEG;
        } else if (degrees > kTurret.MAX_ROTATE_DEG) {
            turretTargetDeg = kTurret.MAX_ROTATE_DEG;
        } else {
            turretTargetDeg = degrees;
        }
    }

    public void updateControlLoops() {
        if (RobotState.isEnabled()) {
            // turret statespace
            double turretTargetRadians = Units.degreesToRadians(turretTargetDeg);
            double turretStateSpaceEffortVolts = calculateStateSpace(turretTargetRadians);

            SmartDashboard.putNumber("output voltage", turretStateSpaceEffortVolts);
            double kS = kTurret.kA * Math.signum(getRateRPM());
            motor.setVoltage(turretStateSpaceEffortVolts + kS);
            NetworkTableInstance.getDefault().flush();
        }
    }

    /**
     * Gets the pose (rotation of turret in degrees) of the turret at a specified
     * execution time.
     * 
     * @param time_ms The time we want to know the pose at.
     * @return Pose of the turret. This should be the degrees the
     *         turret was at at the specified input time.
     */
    public double getPoseAtTime(double time_ms) {
        return buffer.getSample(time_ms);
    }

    // end of latency comp stuff

    private double calculateStateSpace(double targetRadians) {
        // Sets the target position of our arm. This is similar to setting the setpoint
        // of a PID controller.
        TrapezoidProfile.State goal;
        goal = new TrapezoidProfile.State(targetRadians, 0.0);

        // Step our TrapezoidalProfile forward 20ms and set it as our next reference
        lastProfiledReference = (new TrapezoidProfile(kTurret.SSConstraints, goal, lastProfiledReference))
                .calculate(kTurret.CONTROL_LOOP_PERIOD);
        turretLoop.setNextR(lastProfiledReference.position, lastProfiledReference.velocity);

        // Correct our Kalman filter's state vector estimate with encoder data.
        turretLoop.correct(VecBuilder.fill(Units.degreesToRadians(getAngleDegrees())));

        // Update our LQR to generate new voltage commands and use the voltages to
        // predict the next state with out Kalman filter.
        turretLoop.predict(kTurret.CONTROL_LOOP_PERIOD);

        // Send the new calculated voltage to the motors.
        double nextVoltage = turretLoop.getU(0);
        return nextVoltage;
    }

    public void stop() {
        motor.set(0);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run

        // divide by 1 million to convert microseconds to seconds
        double currentTime_s = RobotController.getFPGATime() / 1e6;
        double turretAngle_deg = getAngleDegrees();
        buffer.addSample(currentTime_s, turretAngle_deg);

        SmartDashboard.putNumber("Turret Angle", getAngleDegrees());

    }
}

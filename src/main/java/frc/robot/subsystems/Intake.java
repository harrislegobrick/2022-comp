// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMaxLowLevel.PeriodicFrame;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kIntake;

public class Intake extends SubsystemBase {
    private final CANSparkMax motor;
    private final DoubleSolenoid pistons;

    /** Creates a new Intake. */
    public Intake() {
        pistons = new DoubleSolenoid(PneumaticsModuleType.REVPH, kIntake.PISTONS[0], kIntake.PISTONS[1]);
        motor = new CANSparkMax(kIntake.LEFT_MOTOR, MotorType.kBrushless);

        // Clear what might have been on the motor controller
        motor.restoreFactoryDefaults();

        // Optimize CAN bus utilization
        motor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 5005);
        motor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 5003);

        // Curret limit the neo550 to prevent the motor burnout
        motor.setSmartCurrentLimit(25);

        // Set the motor idle mode
        motor.setIdleMode(IdleMode.kCoast);

        // Set motor inversion
        motor.setInverted(kIntake.REVERSED);

        // Make settings stick
        motor.burnFlash();

        stow();
    }

    /**
     * Makes the intake grab a ball.
     */
    public void run() {
        motor.set(kIntake.SPEED);
    }

    /**
     * Runs the intake motor in the reverse direction. Launching cargo away from the
     * robot?
     */
    public void runReverse() {
        motor.set(-kIntake.SPEED);
    }

    /**
     * Stops the intake motor from running.
     */
    public void stop() {
        motor.stopMotor();
    }

    /**
     * Drops the intake from its stowed position.
     */
    public void deploy() {
        pistons.set(Value.kReverse);
    }

    /**
     * Retracts the intake from being extended to its stowed position.
     */
    public void stow() {
        pistons.set(Value.kForward);
    }

    /**
     * Stops the intake pistons from pushing in any one direction. Used for when the
     * intake is deployed to allow the intake to maintain flexibility when intaking
     * cargo.
     */
    public void limp() {
        pistons.set(Value.kOff);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }
}

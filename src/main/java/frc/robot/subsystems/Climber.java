// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMaxLowLevel.PeriodicFrame;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kClimber;

public class Climber extends SubsystemBase {
    private final CANSparkMax leftMotor, rightMotor;

    /** Creates a new Climber. */
    public Climber() {
        leftMotor = new CANSparkMax(kClimber.LEFT_MOTOR, MotorType.kBrushless);
        rightMotor = new CANSparkMax(kClimber.RIGHT_MOTOR, MotorType.kBrushless);

        leftMotor.restoreFactoryDefaults();
        leftMotor.setInverted(kClimber.INVERTED);
        leftMotor.setIdleMode(IdleMode.kBrake);
        leftMotor.setSmartCurrentLimit(60);
        // CAN bus optimization
        // leftMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 500);
        leftMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 500);
        leftMotor.burnFlash();

        rightMotor.restoreFactoryDefaults();
        rightMotor.setIdleMode(IdleMode.kBrake);
        rightMotor.setSmartCurrentLimit(60);
        rightMotor.follow(leftMotor, true);

        // CAN bus optimization
        // rightMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 500);
        rightMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 500);
        rightMotor.burnFlash();
    }

    public void climb() {
        leftMotor.set(kClimber.SPEED);
    }

    public void stop() {
        leftMotor.set(0.0);
    }

    public void extend() {
        leftMotor.set(-kClimber.SPEED);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }
}

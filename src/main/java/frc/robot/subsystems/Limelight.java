// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import org.photonvision.PhotonUtils;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kVision;

public class Limelight extends SubsystemBase {
    private final NetworkTable table;

    /** Creates a new VisionCamera. */
    public Limelight() {
        table = NetworkTableInstance.getDefault().getTable("limelight");

        table.getEntry("stream").setNumber(2);
    }

    /**
     * Mainly used to set the RPM of the shooter to an approprate speed for the
     * distance.
     * Gets the distance from the robot to the HUB.
     * <p>
     * The units this returns are in
     * meters as the trig is done in meters.
     * 
     * @return Meters from the HUB or if the camera does not detect anything, the
     *         value will be {@code Double.NaN}.
     */
    public double getHUBDistance() {
        if (hasTarget()) {
            double range = PhotonUtils.calculateDistanceToTargetMeters(kVision.LIMELIGHT_HEIGHT,
                    kVision.HUB_TARGET_HEIGHT_METERS, kVision.HUB_CAMERA_PITCH_RADIANS,
                    Units.degreesToRadians(getY()));
            return range;
        } else {
            return Double.NaN;
        }
    }

    /**
     * <b>Experimental second version which should be more accurate when the camera
     * is not pointed directly at the target</b>.
     * <p>
     * Mainly used to set the RPM of the shooter to an approprate speed for the
     * distance.
     * Gets the distance from the robot to the HUB.
     * <p>
     * The units this returns are in
     * meters as the trig is done in meters.
     * 
     * @return Meters from the HUB or if the camera does not detect anything, the
     *         value will be {@code Double.NaN}.
     */
    public double getHUBDistance2() {
        if (hasTarget()) {
            double ty = Units.degreesToRadians(getY());
            double tx = Units.degreesToRadians(getX());
            // eqn derived from
            // https://www.chiefdelphi.com/t/limelight-distance-calculation-process-question/403297/17
            double distance = (kVision.HUB_TARGET_HEIGHT_METERS -
                    kVision.LIMELIGHT_HEIGHT)
                    / (Math.tan(ty + kVision.HUB_CAMERA_PITCH_RADIANS) * Math.cos(tx));
            return distance;
        } else {
            return Double.NaN;
        }
    }

    /**
     * Sets the camera into tracking mode where the LEDs on the limelight are on,
     * the exposure is lowered, and tracking starts.
     * <p>
     * Don't leave this on because you are not supposed to leave bright LEDs on;
     * only enable when you need to track.
     */
    public void setTracking() {
        table.getEntry("ledMode").setNumber(3);
        table.getEntry("camMode").setNumber(0);

        table.getEntry("stream").setNumber(1);
    }

    /**
     * Sets the camera into driving mode where the LEDs on the limelight are off,
     * the exposure is increased, and tracking is disabled.
     * <p>
     * Consider this the "default" state as the LEDs are not supposed to be left on.
     */
    public void setDriving() {
        table.getEntry("ledMode").setNumber(1);
        table.getEntry("camMode").setNumber(1);

        table.getEntry("stream").setNumber(2);
    }

    /**
     * @return Horizontal offset from crosshair to target (-29.8 to 29.8 degrees)
     */
    public double getX() {
        return table.getEntry("tx").getDouble(0.0);
    }

    /**
     * @return The pipeline's latency contribution in seconds.
     */
    public double getLatency() {
        // The table returns the pipeline's latency in milliseconds and then the docs
        // recommend to "Add at least 11ms for image capture latency."
        // Then dividing by one thousand to convert from milliseconds to seconds (SI).
        double additionalLatency = 10.0;
        return (table.getEntry("tl").getDouble(-additionalLatency) + additionalLatency) / 1_000.0;
    }

    /**
     * @return Vertical offset from crosshair to target (-24.85 to 24.85 degrees)
     */
    public double getY() {
        return table.getEntry("ty").getDouble(0.0);
    }

    /**
     * @return Skew or rotation (-90 to 0 degrees)
     */
    public double getSkew() {
        return table.getEntry("ts").getDouble(0.0);
    }

    /**
     * 
     * @return Target area (0% of image to 100% of image)
     */
    public double getArea() {
        return table.getEntry("ta").getDouble(0.0);
    }

    /**
     * @return Results of a 3D position solution, 6 numbers: Translation (x,y,z)
     *         Rotation (pitch, yaw, roll)
     */
    public double[] getPos() {
        return table.getEntry("camtran").getDoubleArray(new double[6]);
    }

    /**
     * @return Whether the limelight has any valid targets ({@code true} if has
     *         target)
     */
    public boolean hasTarget() {
        return table.getEntry("tv").getDouble(0.0) == 1;
    }

    /**
     * @return Whether the limelight is detected by the robot ({@code true} if
     *         detected)
     */
    public boolean isDetected() {
        return getLatency() > 0;
    }

    /**
     * Takes a snapshot of what the limelight sees. This is used to help aid
     * calibration and can help troubleshoot poor thresholding.
     */
    public void takeSnapshot() {
        table.getEntry("snapshot").setNumber(1);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler running
        SmartDashboard.putNumber("HUB Distance", Units.metersToFeet(getHUBDistance()));
        SmartDashboard.putNumber("HUB Distance2", Units.metersToFeet(getHUBDistance2()));
    }
}

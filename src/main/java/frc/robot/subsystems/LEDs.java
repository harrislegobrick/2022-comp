// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LEDs extends SubsystemBase {
    private final PowerDistribution pdh;

    /** Creates a new LEDs. */
    public LEDs() {
        pdh = new PowerDistribution();
        pdh.clearStickyFaults();
        turnOff();
    }

    public void turnOn() {
        pdh.setSwitchableChannel(true);
    }

    public void turnOff() {
        pdh.setSwitchableChannel(false);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }
}

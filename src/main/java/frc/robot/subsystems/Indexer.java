// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMaxLowLevel.PeriodicFrame;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kIndexer;

public class Indexer extends SubsystemBase {
    // inMotor is the motor that drives the wheels to intake the ball into the
    // indexer
    // backMotor is the motor that drives the ball up the indexer
    private final CANSparkMax inMotor, backMotor;

    /** Creates a new Indexer. */
    public Indexer() {
        inMotor = new CANSparkMax(kIndexer.IN_MOTOR, MotorType.kBrushless);
        backMotor = new CANSparkMax(kIndexer.BACK_MOTOR, MotorType.kBrushed);

        // reset victors
        inMotor.restoreFactoryDefaults();
        backMotor.restoreFactoryDefaults();

        // Current limiting
        inMotor.setSmartCurrentLimit(26);
        backMotor.setSmartCurrentLimit(26);

        // Idle mode
        inMotor.setIdleMode(IdleMode.kBrake);
        backMotor.setIdleMode(IdleMode.kBrake);

        // Makes the back motor follow the inverse of what the in motor is doing
        // (following and inversion)
        backMotor.follow(inMotor, true);

        // CAN bus optimization (we don't care about encoder data because there is none)
        inMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 500);
        inMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 500);
        backMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 500);
        backMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 500);

        // Makes the settings "stick"
        inMotor.burnFlash();
        backMotor.burnFlash();
    }

    /**
     * Runs the indexer.
     */
    public void run() {
        inMotor.set(kIndexer.SPEED);
    }

    /**
     * Runs the indexer at a slower speed.
     */
    public void runSlow() {
        inMotor.set(kIndexer.SPEED * 0.65);
    }

    /**
     * Runs the indexer in reverse.
     */
    public void reverse() {
        inMotor.set(-kIndexer.SPEED);
    }

    /**
     * Stops the indexer from moving.
     */
    public void stop() {
        inMotor.set(0.0);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }
}

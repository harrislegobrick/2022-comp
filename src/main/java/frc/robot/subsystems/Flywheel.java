// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMaxLowLevel.PeriodicFrame;

import edu.wpi.first.math.Nat;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.controller.LinearQuadraticRegulator;
import edu.wpi.first.math.estimator.KalmanFilter;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.system.LinearSystem;
import edu.wpi.first.math.system.LinearSystemLoop;
import edu.wpi.first.math.system.plant.LinearSystemId;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.kFlywheel;

/** Creates a new Flywheel. */
public class Flywheel extends SubsystemBase {
    // left and right are observed from the direction the hood faces
    private final CANSparkMax leftMotor, rightMotor;
    private final Encoder throughBoreEncoder;

    // The plant holds a state-space model of our flywheel. This system has the
    // following properties:
    // States: [velocity], in radians per second.
    // Inputs (what we can "put in"): [voltage], in volts.
    // Outputs (what we can measure): [velocity], in radians per second.
    private final LinearSystem<N1, N1, N1> flywheelPlant = LinearSystemId.identifyVelocitySystem(kFlywheel.kV,
            kFlywheel.kA);

    private final KalmanFilter<N1, N1, N1> observer = new KalmanFilter<>(Nat.N1(), Nat.N1(), flywheelPlant,
            VecBuilder.fill(3.0), // how accurate we think our model is
            VecBuilder.fill(0.03), // how accurate we think our encoder data is
            kFlywheel.CONTROL_LOOP_PERIOD);

    private final LinearQuadraticRegulator<N1, N1, N1> controller = new LinearQuadraticRegulator<>(flywheelPlant,
            VecBuilder.fill(0.5), // qelms. Velocity error tolerance, in radians per second. Decrease this to more
                                  // heavily penalize state excursion, or make the controller behave more
                                  // aggressively
            VecBuilder.fill(12.0), // relms. Control effort (voltage) tolerance. Decrease this to more
            // heavily penalize control effort, or make the controller less aggressive. 12
            // is a good starting point because that is the (approximate) maximum voltage of
            // a battery.
            kFlywheel.CONTROL_LOOP_PERIOD); // Nominal time between loops. 0.020 for TimedRobot, but can be lower if
                                            // using
    // notifiers.

    // The state-space loop combines a controller, observer, feedforward and plant
    // for easy control.
    private final LinearSystemLoop<N1, N1, N1> loop = new LinearSystemLoop<>(flywheelPlant, controller, observer, 12.0,
            kFlywheel.CONTROL_LOOP_PERIOD);

    /**
     * Creates a new Flywheel.
     */
    public Flywheel() {
        throughBoreEncoder = new Encoder(kFlywheel.ENCODER_PORT[0],
                kFlywheel.ENCODER_PORT[1], kFlywheel.ENCODER_INVERTED, EncodingType.k4X);
        throughBoreEncoder.setDistancePerPulse(kFlywheel.ENCODER_CONVERSION);
        // throughBoreEncoder.setSamplesToAverage(10);

        leftMotor = new CANSparkMax(kFlywheel.LEFT_MOTOR, MotorType.kBrushless);
        rightMotor = new CANSparkMax(kFlywheel.RIGHT_MOTOR, MotorType.kBrushless);

        leftMotor.restoreFactoryDefaults();
        leftMotor.setIdleMode(IdleMode.kCoast);
        leftMotor.setInverted(kFlywheel.INVERTED);
        leftMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 20); // using encoder so we don't care about velocity
        leftMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 500); // don't care about position
        leftMotor.setSmartCurrentLimit(80);
        leftMotor.burnFlash();

        rightMotor.restoreFactoryDefaults();
        rightMotor.setIdleMode(IdleMode.kCoast);
        rightMotor.setInverted(!kFlywheel.INVERTED);
        rightMotor.setSmartCurrentLimit(80);
        rightMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus1, 500); // follower so we don't care about data
        rightMotor.setPeriodicFramePeriod(PeriodicFrame.kStatus2, 500);
        rightMotor.burnFlash();

        // May be needed idk
        // Adjust our LQR's controller for 25 ms of sensor input delay. We
        // provide the linear system, discretization timestep, and the sensor
        // input delay as arguments.
        controller.latencyCompensate(flywheelPlant, kFlywheel.CONTROL_LOOP_PERIOD,
                0.0028);

        loop.reset(VecBuilder.fill(0.0));

    }

    /**
     * Max RPM of a NEO is 5676 RPM.
     * 
     * @param rpm The rpm to set the motor to.
     */
    public void setRPM(double rpm) {
        double radIn = Units.rotationsPerMinuteToRadiansPerSecond(rpm);
        loop.setNextR(VecBuilder.fill(radIn));
        SmartDashboard.putNumber("Target Flywheel RPM", rpm);
    }

    /**
     * Really only used for debug purposes since it's simple percent and not motor
     * RPM.
     * <p>
     * Does not use the LQR for motor RPM control.
     * 
     * @param speed Values from [-1, 1] where -1 is full reverse, 0 is stop, and 1
     *              is full forward
     */
    public void setMotorPercent(double speed) {
        leftMotor.set(speed);
        rightMotor.set(speed);
    }

    /**
     * Get the velocity of the flywheel motor
     * 
     * @return The velocity in radians per second.
     */
    public double getRate() {
        return throughBoreEncoder.getRate();
    }

    public double getFilteredRate() {
        return loop.getXHat(0);
    }

    /**
     * Stops the flywheel from spinning.
     */
    public void stop() {
        loop.setNextR(VecBuilder.fill(0.0));
    }

    /**
     * This updates the control loops that are controlling both of the flywheel
     * motors. This is delegated to a seperate method so that we can update it more
     * frequently than the standard roboRIO update rate of 50 times a second (the
     * control loop period is
     * 0.02).
     * <p>
     * Specifically, the update rate is set at
     * {@link Constants.kFlywheel.CONTROL_LOOP_PERIOD}.
     */
    public void updateControlLoops() {
        if (RobotState.isEnabled()) {
            // Correct our Kalman filter's state vector estimate with encoder data.
            loop.correct(VecBuilder.fill(getRate()));
            // Update our LQR to generate new voltage commands and use the voltages to
            // predict the next state with out Kalman filter.
            loop.predict(kFlywheel.CONTROL_LOOP_PERIOD);

            // sets motor voltage to the loop's calculation
            double voltage = loop.getU(0);
            if (loop.getNextR(0) != 0) {
                leftMotor.setVoltage(voltage);
                rightMotor.setVoltage(voltage);
            } else {
                leftMotor.set(0.0);
                rightMotor.set(0.0);
            }
            // NetworkTableInstance.getDefault().flush(); uncomment for graphing

        }
        SmartDashboard.putNumber("Kalman filter RPM", Units.radiansPerSecondToRotationsPerMinute(loop.getXHat(0)));
        SmartDashboard.putNumber("Encoder RPM", Units.radiansPerSecondToRotationsPerMinute(getRate()));
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run

        // observe motor RPM and filtered RPM
        SmartDashboard.putNumber("Motor RPM (will be slow)", leftMotor.getEncoder().getVelocity());

    }

}
